#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
<header>
  <input>../kkmc_gen.dst.root</input>
  <output>../both_tau_to_3pi.ntup.root</output>
  <contact>mail@vidyasagarv.com</contact>
</header>
"""

###############################################################
#
# Analysis Code (make Ntuple for validation of tau->pi pi pi
# contributor : Vidya Sagar V (LAL, Paris-Saclay Univ.)
#               mail@vidyasagarv.com  (Feb 2020)
#
################################################################

import os
import basf2
from modularAnalysis import *
from variables import variables
from ROOT import Belle2
from stdCharged import *
import variables.collections as vc
import variables.utils as vu

myPath = basf2.Path()

# set the input files
inputMdstList(environmentType='default', filelist='./kkmc_gen.dst.root', path=myPath)

outputFile='both_tau_to_3pi.ntup.root' #(name_of_the_file)

stdPi('all',path=myPath) # function to prepare standard pion lists, all : with no cuts on track

#cutAndCopyList('pi+:sig', 'pi+:all', 'chiProb>0.001 and abs(d0)<2 and abs(z0)<4 and nCDCHits>=20 and nSVDHits>=6',path=my_path)
cutAndCopyList('pi+:sig', 'pi+:all', '',path=myPath)

# reconstruction : tau -> mu mu mu
reconstructDecay('tau+:all -> pi+:all pi-:all pi+:all', '1.5 < M < 2.1',path=myPath)

# Reconstruct the pair of tau candidates from a virtual photon (vpho)
ma.reconstructDecay('vpho:all -> tau+:all tau-:all', '', path=myPath)

# MC matching of the tau candidates
matchMCTruth('tau+:all', path=myPath)

# Information of the generated decay mode
labelTauPairMC(path=myPath)

eventVariables = ['missingMomentumOfEvent',
                  'missingMomentumOfEvent_theta',
                  'missingMomentumOfEventCMS',
                  'missingMomentumOfEventCMS_theta',
                  'tauPlusMCMode',
                  'tauMinusMCMode']

# Store also some useful MC match information.
mcVariables = ['mcErrors',
               'genMotherPDG',
               'mcPDG']

kVariables = vc.inv_mass + vc.kinematics

# Create the variable list
variableList = vu.create_aliases_for_selected(list_of_variables=eventVariables,
                                              decay_string='^vpho') + \
               vu.create_aliases_for_selected(list_of_variables=mcVariables + kVariables + ['charge'],
                                              decay_string='vpho -> ^tau+ ^tau-')

variablesToNtuple(decayString='vpho:all',
                  variables=variableList,
                  filename=outputFile,
                  treename='tau',
                  path=myPath)

# Process the events
basf2.process(path=myPath)

# print out the summary
print(basf2.statistics)

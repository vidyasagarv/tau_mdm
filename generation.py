#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Descriptor: e+e- --> (tau+ --> pi pi pi anti-nu) (tau- --> pi pi pi nu)

"""
<header>
  <output>../kkmc_gen.dst.root</output>
  <contact>mail@vidyasagarv.com</contact>
</header>
"""

from basf2 import *
from simulation import add_simulation
from reconstruction import add_reconstruction, add_mdst_output
from ROOT import Belle2
import sys
import glob

set_random_seed(12345)

# main path
main = create_path()

# event info setter
eventinfosetter = register_module('EventInfoSetter')
eventinfosetter.param('evtNumList', [1000])
eventinfosetter.param('runList', [0])
eventinfosetter.param('expList', [0])
main.add_module(eventinfosetter)

# to run the framework the used modules need to be registered
kkgeninput = register_module('KKGenInput')

kkgeninput.param('tauinputFile', './tau.input.dat')
kkgeninput.param('KKdefaultFile', './KK2f_defaults.dat')
kkgeninput.param('taudecaytableFile', './both_tau_to_3pi.dat')
# Tip: The number of spaces between columns in dat file simetimes matter.

main.add_module(kkgeninput)

# detector simulation
add_simulation(main)

# reconstruction
add_reconstruction(main)

# Finally add mdst output
add_mdst_output(main, filename='./kkmc_gen.dst.root')

# generate events
process(main)
print(statistics)

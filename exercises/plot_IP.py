#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import root_pandas

# Read the root file into a pandas dataframe
df = root_pandas.read_root('IP_profile_1.ntup.root', key='D0')

# Pring the IP info if there is only one, if not plot them.

if df.IPX_in_um.unique().__len__() == 1:
	print("IP x-coordinate = {} um".format(df.IPX_in_um.unique()[0]))
else:
	df.IPX_in_um.hist(range=(-1,1), bins=100, xrot = 90)
	plt.savefig('IPX.pdf')

if df.IPY_in_um.unique().__len__() == 1:
	print("IP y-coordinate = {} um".format(df.IPY_in_um.unique()[0]))
else:
	df.IPY_in_um.hist(range=(-1,1), bins=100, xrot = 90)
	plt.savefig('IPY.pdf')

if df.IPZ_in_um.unique().__len__() == 1:
	print("IP z-coordinate = {} um".format(df.IPZ_in_um.unique()[0]))
else:
	df.IPZ_in_um.hist(range=(-10,10), bins=100, xrot = 90)
	plt.savefig('IPZ.pdf')

if df.IPX_sigma_in_um.unique().__len__() == 1:
	print("IP x-spread = {} um".format(df.IPX_sigma_in_um.unique()[0]))
else:
	df.IPX_sigma_in_um.hist(range=(1,100), bins=100, xrot = 90)
	plt.savefig('IPX_sigma.pdf')

if df.IPY_sigma_in_um.unique().__len__() == 1:
	print("IP y-spread = {} um".format(df.IPY_sigma_in_um.unique()[0]))
else:
	df.IPY_sigma_in_um.hist(range=(1,100), bins=100, xrot = 90)
	plt.savefig('IPY_sigma.pdf')

if df.IPZ_sigma_in_um.unique().__len__() == 1:
	print("IP z-spread = {} um".format(df.IPZ_sigma_in_um.unique()[0]))
else:
	df.IPZ_sigma_in_um.hist(range=(100,200), bins=100, xrot = 90)
	plt.savefig('IPZ_sigma.pdf')

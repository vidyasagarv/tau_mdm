#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import basf2
import modularAnalysis as ma
from variables import variables as vm
from stdCharged import *
from beamparameters import add_beamparameters

# Create a basf2 path
myPath = basf2.Path()

# Take an additional argument to select the prod file among the 5 in MC folder
ext = sys.argv[1]

# Set the input files
ma.inputMdstList(environmentType='default', filelist='/group/belle/users/vsagar/D0ToHpJm_Skim_Output/prod00007456_'+str(ext)+'.udst.root', path=myPath)

# Define the name of the output file
outputFile = 'IP_profile_'+str(ext)+'.ntup.root'

# Generate a list of pions and kaons
stdPi('all', path=myPath)
stdK('all', path=myPath)

# Reconsrtuct D0 from pions and kaons
ma.reconstructDecay(decayString='D0:all -> pi+:all K-:all', cut='', path=myPath)

# MC truth matching
ma.matchMCTruth(list_name='D0:all', path=myPath)

# List of variables to be saved in the output root file. Includes: IP coordinates, sqrt(diagonal elements of covariance matrix) = widths of beam spot and multiply them by e4 to convert to micrometer from centimeter
#eventVariables = ['EventType', 'Ecms', 'IPX', 'IPY', 'IPZ', 'sqrt(IPCov(0,0))', 'sqrt(IPCov(1,1))', 'sqrt(IPCov(2,2))']
eventVariables = ['EventType', 'InvM', 'Ecms']
vm.addAlias('IPX_in_um', 'formula(IPX*10000)')
vm.addAlias('IPY_in_um', 'formula(IPY*10000)')
vm.addAlias('IPZ_in_um', 'formula(IPZ*10000)')
vm.addAlias('IPX_sigma_in_um', 'formula((IPCov(0,0)^0.5)*10000)')
vm.addAlias('IPY_sigma_in_um', 'formula((IPCov(1,1)^0.5)*10000)')
vm.addAlias('IPZ_sigma_in_um', 'formula((IPCov(2,2)^0.5)*10000)')
eventVariables += ['IPX_in_um', 'IPY_in_um', 'IPZ_in_um','IPX_sigma_in_um', 'IPY_sigma_in_um', 'IPZ_sigma_in_um']

# Convert the variables in to Ntuples and save them
ma.variablesToNtuple(decayString='D0:all',
									variables=eventVariables,
									filename=outputFile,
									treename='D0',
									path=myPath)

# Print beam parameters
beamparameters = add_beamparameters(myPath, "Y4S")
basf2.print_params(beamparameters)

# Run the process
basf2.process(path=myPath)

# Print standard statistics
print(basf2.statistics)
